import jsonschema
import anyconfig
from path import Path

schema = anyconfig.load(Path("gpc") / "schema" / "config.schema.yaml")
jsonschema.Draft7Validator.check_schema(schema)
anyconfig.dump(schema, out="dist/config-schema-v1.json", ac_parser="json", indent=2)
