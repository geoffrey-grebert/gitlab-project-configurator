# Third Party Libraries
import pytest

from dictns import Namespace

# Gitlab-Project-Configurator Modules
from gpc.executors.project_setting_executor import DefaultBranchUpdator


@pytest.mark.parametrize(
    "force_create_default_branch, ",
    [True, False],
)
def test_branch_treatment(mocker, force_create_default_branch):
    rule = Namespace(
        {"default_branch": "branch", "force_create_default_branch": force_create_default_branch}
    )
    mock_create_default_branch = mocker.Mock(return_value=True)
    mock_exist_branch = mocker.Mock(return_value=True)
    DefaultBranch = DefaultBranchUpdator(
        project=mocker.Mock(),
        rule=rule,
        show_diff_only=False,
    )
    DefaultBranch.create_default_branch = mock_create_default_branch
    DefaultBranch.exist_branch = mock_exist_branch
    DefaultBranch.update()
    if force_create_default_branch:
        assert DefaultBranch.create_default_branch.called
        assert not DefaultBranch.exist_branch.called
    else:
        assert not DefaultBranch.create_default_branch.called
        assert DefaultBranch.exist_branch.called
